const posts = [
    {title: 'Post one', body:"This is first post"},
    {title: 'Post two', body:"This is second post"}
];

function getPosts(){
    //setTimeout takes in a callback fun and time to delay
    setTimeout(() => {
        let output = '';
        posts.forEach((post, index)=>{
            output += `<li>${post.title}</li>`;
            document.body.innerHTML = output;
        });
    }, 1000);   //Creating timeout of 1000ms
}

function createPost(post){
    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            posts.push(post);
            const error = false;
            if(!error){
                resolve();
            }
            else{
                reject('Error: something went wrong.');
            }
        }, 2000);
    });   
}

/*
createPost({title:"Post three", body:"This is the third post"})
.then(getPosts)
.catch(err => console.log(err));
*/

/*
// Async/Await
// Function should be labelled async if you need to use await inside of it.
async function init() {
    await createPost({title:"Post three", body:"This is the third post"});
    getPosts(); //This will only execute after the createPost is completed.
}
init();
*/

//Async / Await/ Fetch
async function fetchUsers(){
    const res = await fetch('https://jsonplaceholder.typicode.com/users');
    const data = await res.json();
    console.log(data);
}
fetchUsers();

/*
// If there are multiple promises then we can use Promise.all
const promise1 = Promise.resolve('Hello world');
const promise2 = 10;
const promise3 = new Promise((resolve,reject) =>
    setTimeout(resolve,2000, "Goodbye"));
// When using fetch() we need two .then() first for formatting json and next to give output.
const promise4 = fetch("https://jsonplaceholder.typicode.com/users").then(res=> res.json());
// Promise.all() takes in an array of promises.
Promise.all([promise1,promise2,promise3, promise4]).then((values)=>{
    console.log(values);
});
*/